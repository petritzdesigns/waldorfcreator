#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QMainWindow>
#include <QMap>
#include <QPointer>
#include <QMessageBox>
#include <QDebug>
#include "highlighter.h"
#include "compiler.h"
#include "runner.h"

QT_BEGIN_NAMESPACE
class QAction;
class QComboBox;
class QFontComboBox;
class QTextEdit;
class QTextCharFormat;
class QMenu;
class QPrinter;
QT_END_NAMESPACE

class TextEdit : public QMainWindow
{
    Q_OBJECT

public:
    TextEdit(QWidget *parent = 0);

protected:
    virtual void closeEvent(QCloseEvent *e);

private:
    void setupFileActions();
    void setupEditActions();
    void setupRunActions();
    bool load(const QString &f);
    bool maybeSave();
    void setCurrentFileName(const QString &fileName);
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);

private slots:
    void fileNew();
    void fileOpen();
    bool fileSave();
    bool fileSaveAs();
    void filePrint();
    void filePrintPreview();
    void filePrintPdf();
    void compileFile();
    void runFile();
    void compileRunFile();

    void clipboardDataChanged();
    void about();
    void printPreview(QPrinter *);

private:
    QAction *actionSave;
    QAction *actionUndo;
    QAction *actionRedo;
    QAction *actionCut;
    QAction *actionCopy;
    QAction *actionPaste;
    QAction *actionCompile;
    QAction *actionRun;
    QAction *actionCompileRun;
    QToolBar *tb;
    QString fileName;
    QTextEdit *textEdit;
    Highlighter *highlighter;
    QString outputFileName;
};

#endif // TEXTEDIT_H
