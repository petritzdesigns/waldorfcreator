#-------------------------------------------------
#
# Project created by QtCreator 2014-10-27T07:47:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
qtHaveModule(printsupport): QT += printsupport

TARGET = WaldorfCreator
TEMPLATE = app


SOURCES += main.cpp\
        textedit.cpp\
        highlighter.cpp \
    compiler.cpp \
    runner.cpp

HEADERS  += textedit.h\
         highlighter.h \
    compiler.h \
    runner.h

RESOURCES += textedit.qrc
build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

EXAMPLE_FILES = textedit.qdoc

# install
#target.path = $$[QT_INSTALL_EXAMPLES]/widgets/richtext/textedit
#INSTALLS += target
