#include "compiler.h"
#include <iostream>

using namespace std;

Compiler::Compiler(QString file)
{
    this->input = file;
}

void Compiler::compile(QString outputFile)
{
    this->outputFileName = outputFile;

    qDebug() << "Compile started...";

    //create output file
    createOutputFile();
    qDebug() << "Output File Name: " << outputFileName << endl;

    //Split to Array
    QStringList lines = input.split("\n", QString::SkipEmptyParts);

    //setup base code
    setupBase();

    currentIndex = 0;

    bool huetteExist = false;
    foreach(QString line, lines) {
        if(line.contains("hütte")) {
            huetteExist = true;
        }
    }
    if(!huetteExist)
    {
        Error error;
        error.atLine = 0;
        error.errorTitle = "Keine Hütte!";
        error.errorMessage = "Es existiert keine Hütte!";
        errors.append(error);
        showError(error);
        return;
    }

    foreach(QString line, lines) {
        if(huetteEnd) {
            break;
        }
        currentIndex++;
        analyseLine(line);
        if(currentIndex == 1 && !buchIsIncluded)
        {
            Error error;
            error.atLine = currentIndex;
            error.errorTitle = "Resource fehlt";
            error.errorMessage = "\"buch.waldorf\" ist nicht eingebunden!";
            errors.append(error);
            showError(error);
            return;
        }
    }
}

void Compiler::analyseLine(QString line)
{
    if(line.startsWith("einbinden"))
    {
        int begin = line.indexOf("\"") + 1;
        int end = line.lastIndexOf("\"") - begin;
        QString import = line.mid(begin, end);
        if(import == "buch.waldorf" && !buchIsIncluded) {
            buchIsIncluded = true;
            addComponent(COMP_IMPORT, "all");
            addComponent(COMP_COPYRIGHT, "long");
        }
    }
    else if(line.startsWith("hütte"))
    {
        className = line.mid(6, line.indexOf(":") - 6);
        huetteSet = true;
        //qDebug() << className;
        addComponent(COMP_CLASS_START, className);
        addComponent(COMP_MAIN_METHOD_START, "main");
        addComponent(COMP_METHOD_START, "start");
        addComponent(COMP_MAIN_METHOD_END, "main");
        addComponent(COMP_METHOD_MAIN, "start");
    }
    else if(line.startsWith(":hütte"))
    {
        huetteEnd = true;
        addComponent(COMP_METHOD_END, "start");
        addComponent(COMP_ADD_BUCH, "all");
        addComponent(COMP_CLASS_END, "std");
    }
    else if(line.startsWith("#"))
    {
        addComponent(COMP_COMMENT, line.mid(1, line.length()));
    }
    else if(line.startsWith("ausgabe("))
    {
        appendToFile(addTab().append(addTab()).append(line).append(";"), true);
    }
    else if(line.startsWith("sack"))
    {
        appendToFile(addTab().append(addTab()).append(line.replace("sack", "int")).append(";"), true);
    }
    else
    {
        Error error;
        error.atLine = currentIndex;
        error.errorTitle = "Nicht vorhandener Befehl!";
        error.errorMessage = QString("Der Befehl: %1 existiert nicht!").arg(line);
        errors.append(error);
        showError(error);
        return;
    }
}

void Compiler::addComponent(int comp, QString extra)
{
    //Imports
    QStringList imports;
    imports.append("import javax.swing.JOptionPane;");
    imports.append("import java.util.Calendar;");
    imports.append("import java.text.SimpleDateFormat;");

    //Copyrights
    QString copyright = QString("/**\n* Compiled .waldorf File\n*\n* @see http://petritzdesigns.com/waldorf.html\n* @version 1.0.0\n*/");
    QString longCopyright = QString("/**\n* Compiled .waldorf File to .java File\n* With a C++ Compiler written by jCorn Development\n* See More at: http://mpdev.biz\n* Or watch the documentation\n*\n* @see http://petritzdesigns.com/waldorf.html\n* @version 1.0.0\n* @author Markus Petritz\n* @author Julian Maierl\n*\n* Copyright (c) jCorn Development\n*/");
    QString smallCopyright = QString("/**\n* Compiled waldorf File\n*/");

    //Class
    QString classStart = QString("public class ");
    QString classEnd = QString(" {");

    //Main Method
    QString mainMethod = QString("%1%2\n%3%4%5 %6 = new %7();").arg(addTab(), "public static void main(String[] args) {", addTab(), addTab(), className, className.toLower(), className);
    QString endMainMethod = QString("%1}").arg(addTab());

    //Method
    QString method = QString("%1%2%3.").arg(addTab(), addTab(), className.toLower());
    QString endMethod = "();";

    //Whole Method
    QString wMethod = QString("%1%2").arg(addTab(), "private void ");
    QString wEndMethod = QString("() {");
    QString weEndMethod = QString("%1}").arg(addTab());

    //Comment
    QString comment = "//";

    //Buch Methods
    QStringList methods;
    methods.append("	private void ausgabe(String aString) {\n		System.out.println(aString);\n	}");
    methods.append("	private void ausgabe(int aNumber) {\n		System.out.println(""+aNumber);\n	}");
    methods.append("	private String zeit() {\n		return new SimpleDateFormat(\"HH:mm:ss\").format(Calendar.getInstance().getTime());\n	}");
    methods.append("	private int rot(int... iNumbers) {\n		int erg = 0;\n		for(int num : iNumbers) {\n			erg += num;\n		}\n		return erg;\n	}");
    methods.append("	private int blau(int z1, int z2) {\n		return z1 - z2;\n	}");

    switch(comp) {
    case 0:
        //Component: Imports
        if(extra == "all")
        {
            foreach (QString import, imports) {
                appendToFile(import, true);
            }
        } else if(extra == "min")
        {
            appendToFile(imports.at(0), true);
            appendToFile(imports.at(1), true);
        }
        break;
    case 1:
        //Component: Copyright
        if(extra == "std")
        {
            appendToFile(copyright, true);
        }
        else if(extra == "long")
        {
            appendToFile(longCopyright, true);
        }
        else if(extra == "small")
        {
            appendToFile(smallCopyright, true);
        }
        break;
    case 2:
        //Component: Start Class
        if(!extra.isEmpty())
        {
            QString classString = classStart.append(extra).append(classEnd);
            appendToFile(classString, true);
        }
        break;
    case 3:
        //Component: End Class
        if(extra == "std")
        {
            appendToFile("}", true);
        }
        break;
    case 4:
        //Component: Main Method
        if(extra == "main")
        {
            appendToFile(mainMethod, true);
        }
        break;
    case 5:
        //Component: Method
        if(!extra.isEmpty())
        {
            appendToFile(method.append(extra).append(endMethod), true);
        }
        break;
    case 6:
        //Component: End Main Method
        if(extra == "main")
        {
            appendToFile(endMainMethod, true);
        }
        break;
    case 7:
        //Component: Whole Method
        if(!extra.isEmpty())
        {
            appendToFile(wMethod.append(extra).append(wEndMethod), true);
        }
        break;
    case 8:
        //Component: End Whole Method
        if(!extra.isEmpty())
        {
            appendToFile(weEndMethod, true);
        }
        break;
    case 9:
        //Component: Comment
        if(!extra.isEmpty())
        {
            QString wComment = QString("%1%2%3%4").arg(addTab(), addTab(), comment, extra);
            appendToFile(wComment, true);
        }
        break;
    case 10:
        //Component: Buch Methods
        if(extra == "all")
        {
            foreach (QString method, methods) {
                appendToFile(method, true);
            }
        } else if(extra == "min")
        {
            appendToFile(methods.at(0), true);
        }
        break;
    }
}

void Compiler::setupBase()
{
    buchIsIncluded = false;
    huetteSet = false;
    huetteEnd = false;
}

void Compiler::createOutputFile()
{
    if(outputFileName.startsWith(":/"))
    {
        outputFileName = QFileDialog::getSaveFileName(NULL, "Speichern als...", QString(),
                                                  "Java-Datei (*.java)");
    }
    else
    {
        outputFileName = QFileDialog::getSaveFileName(NULL, "Speichern als...", QString(),
                                                       "Java-Datei (*.java)");
    }

    QFile file(outputFileName);
    if(file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream stream(&file);
        QDate date = QDate::currentDate();
        stream << "//Created: " << date.toString() << endl;
    }

    qDebug() << "Created Output File";
}

void Compiler::appendToFile(QString string, bool newLine)
{
    QFile file(outputFileName);
    if(file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        if(newLine)
        {
            stream << string << endl;
            //qDebug() << string << endl;
        }
        else
        {
            stream << string;
            //qDebug() << string;
        }
    }
}

QString Compiler::addTab()
{
    return "	";
}

void Compiler::showError(Error errorToShow)
{
    QString message = QString("%1 \nbei Zeile: %2").arg(errorToShow.errorMessage, QString::number(errorToShow.atLine));
    QMessageBox::information(NULL, errorToShow.errorTitle, message, QMessageBox::Ok, QMessageBox::Cancel);
}
