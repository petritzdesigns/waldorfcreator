#ifndef COMPILER_H
#define COMPILER_H

#include <QString>
#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QDate>
#include <QDataStream>
#include <QMessageBox>

#define COMP_IMPORT 0
#define COMP_COPYRIGHT 1
#define COMP_CLASS_START 2
#define COMP_CLASS_END 3
#define COMP_MAIN_METHOD_START 4
#define COMP_METHOD_START 5
#define COMP_MAIN_METHOD_END 6
#define COMP_METHOD_MAIN 7
#define COMP_METHOD_END 8
#define COMP_COMMENT 9
#define COMP_ADD_BUCH 10

class Compiler
{
public:
    Compiler(QString input);
    void compile(QString outputFile);


private:
    struct Error
    {
        int atLine;
        QString errorTitle;
        QString errorMessage;
    };

private:
    QString input;
    QString outputFileName;
    QString className;
    QVector<Error> errors;

    bool buchIsIncluded;
    bool huetteSet;
    bool huetteEnd;

    int currentIndex;

private:
    void createOutputFile();
    void setupBase();
    void appendToFile(QString string, bool newLine);
    void analyseLine(QString line);
    void addComponent(int comp, QString extra);
    void showError(Error error);
    QString addTab();
};

#endif // COMPILER_H
