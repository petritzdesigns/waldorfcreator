#include "runner.h"

Runner::Runner(QString ioutputFileName)
{
    qDebug() << "Runner started";
    if(ioutputFileName.startsWith(":/"))
    {
        ioutputFileName = QFileDialog::getSaveFileName(NULL, "Speichern als...", QString(),
                                                  "Java-Datei (*.java)");
    }
    else
    {
        ioutputFileName = QFileDialog::getSaveFileName(NULL, "Speichern als...", QString(),
                                                       "Java-Datei (*.java)");
    }
    this->outputFileName = ioutputFileName;
}

void Runner::run()
{
    compile();
    execute();
}

void Runner::compile()
{
    QProcess *compileProcess = new QProcess(NULL);
    QString arg = "javac";
    compileProcess->start(arg, QStringList() << outputFileName);
    compileProcess->waitForFinished();
    QByteArray output = compileProcess->readAll();

    if(!output.isEmpty() || compileProcess->exitCode() > 0)
    {
        QString message = "Es ist ein unerwarteter Fehler aufgetreten :";
        message.append(output);
        QMessageBox::information(NULL, "Fehler...", message, QMessageBox::Ok, QMessageBox::Cancel);
        return;
    }

    qDebug() << "Output: " << output;
    qDebug() << "ExitCode: " << compileProcess->exitCode();
    qDebug() << "Compile: " << arg << outputFileName;
    qDebug() << "Process" << compileProcess->arguments();
}

void Runner::execute()
{
    QProcess *runProcess = new QProcess(NULL);
    QString arg = "java";
    QString name = outputFileName.left(outputFileName.indexOf("."));
    QString fileName = name.mid(name.lastIndexOf("/")+1, name.length());
    QString directory = name.left(name.lastIndexOf("/")+1);
    runProcess->start(arg, QStringList() << "-classpath" << directory << fileName);
    runProcess->waitForFinished();
    QByteArray output = runProcess->readAll();

    qDebug() << "Output: " << output;
    qDebug() << "ExitCode: " << runProcess->exitCode();
    qDebug() << "Run: " << arg << " " << directory << " " << fileName;
    qDebug() << "Process" << runProcess->arguments();

    QMessageBox::information(NULL, "Output of Program: ", output, QMessageBox::Ok, QMessageBox::Cancel);
}
