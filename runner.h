#ifndef RUNNER_H
#define RUNNER_H

#include <QString>
#include <QDebug>
#include <QProcess>
#include <QFileDialog>
#include <QMessageBox>

class Runner
{
public:
    Runner(QString outputFileName);
    void run();

private:
    void compile();
    void execute();

private:
    QString outputFileName;
};

#endif // RUNNER_H
